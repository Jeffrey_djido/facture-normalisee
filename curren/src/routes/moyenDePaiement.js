const express = require('express');
const router = express.Router();
const moyenPaiementCtrl = require('../controllers/moyenDePaiement');


// Ajouter une balance commerciale
router.post('/moyenDePaiement', moyenPaiementCtrl.createMoyenDePaiement)


//Afficher une blance commerciale

router.get("/moyenDePaiement/:_id", moyenPaiementCtrl.getMoyenDePaiement);



// Modifier une balance commerciale
router.put("/moyenDePaiement/:_id", moyenPaiementCtrl.updateMoyenDePaiement);
router.patch("/moyenDePaiement/:_id", moyenPaiementCtrl.updateMoyenDePaiement);



// Supprimer une balance commerciale
router.delete("/moyenDePaiement/:_id", moyenPaiementCtrl.deleteMoyenDePaiement);











// ajouter un moyen de paiement
router.post('/moyenPaiement', (req, res) => {
    const {id_Paie, libelle_Paie, method_Paie} = req.body;
    // créer un nouveau moyen de paiement
    const newMoyenPaiement = {
        id_Paie, 
        libelle_Paie, 
        method_Paie
    };

    // ajouter le moyen de paiement à la liste des moyens de paiement
    moyensPaiement.push(newMoyenPaiement);

    res.json(newMoyenPaiement);
});

// modifier un moyen de paiement
router.put('/moyenPaiement/:id', (req, res) => {
    const { id } = req.params;
    const { libelle_Paie, method_Paie } = req.body;

    const moyenPaiementIndex = moyensPaiement.findIndex(m => m.id_Paie == id);

    if (moyenPaiementIndex === -1) {
        return res.status(404).json({ msg: 'Moyen de paiement non trouvé' });
    }

    const updatedMoyenPaiement = {
        id_Paie: moyensPaiement[moyenPaiementIndex].id_Paie,
        libelle_Paie,
        method_Paie
    };

    moyensPaiement[moyenPaiementIndex] = updatedMoyenPaiement;

    res.json(updatedMoyenPaiement);
});

// supprimer un moyen de paiement
router.delete('/moyenpaiement/:id', (req, res) => {
    const { id } = req.params;

    const moyenPaiementIndex = moyensPaiement.findIndex(m => m.id_Paie == id);

    if (moyenPaiementIndex === -1) {
        return res.status(404).json({ msg: 'Moyen de paiement non trouvé' });
    }

    moyensPaiement.splice(moyenPaiementIndex, 1);

    res.json({ msg: 'Moyen de paiement supprimé' });
});

module.exports = router;