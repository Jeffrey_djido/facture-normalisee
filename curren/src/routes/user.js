const express = require("express");
const router = express.Router();

const limiter = require('../middlewares/express-rate-limit'); 
const userCtrl = require("../controllers/user");
const passwordValidator = require('../middlewares/password-validator'); 

router.post("/signup", passwordValidator, userCtrl.signup);
router.post("/login", limiter, userCtrl.login);

module.exports = router;
