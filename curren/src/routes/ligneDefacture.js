const express = require('express');
const router = express.Router();
const ligneDeFactureCtrl = require("../controllers/ligneDefacture")
const ArticleCtrl = require('../controllers/article');
const auth = require("../middlewares/auth");


//ajouter un article
router.get("/article/:_id", ArticleCtrl.getArticle);
router.put("/article/:_id", ArticleCtrl.updateArticle);

// ajouter une ligne de facture
router.post("/ligneDefacture", ligneDeFactureCtrl.createLigneFacture);




// modifier une ligne de facture
router.put("/ligneDefacture/:_id", ligneDeFactureCtrl.updateLigneFacture);
router.patch("/ligneDefacture/:_id", ligneDeFactureCtrl.updateLigneFacture);


// supprimer une ligne de facture

router.delete("/ligneDefacture/:id", ligneDeFactureCtrl.deleteLigneFacture);


module.exports = router;
