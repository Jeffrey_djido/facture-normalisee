const express = require('express');
const router = express.Router();
const clientCtrl = require('../controllers/client');
const Client= require('../models/Client');

// Ajouter un client
router.post('/client', clientCtrl.createClient)


//Afficher un client

router.get("/client/:_id", clientCtrl.getClient);
router.get("/client", clientCtrl.getAllClient);


// Modifier un client
router.put("/client/:_id", clientCtrl.updateClient);
router.patch("/client/:_id", clientCtrl.updateClient);



// Supprimer un Client
router.delete("/client/:_id", clientCtrl.deleteClient);



module.exports = router;