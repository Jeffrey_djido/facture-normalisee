const express = require('express');
const router = express.Router();
const categoryCtrl = require('../controllers/Category');
const Client= require('../models/Client');

// Ajouter un client
router.post('/Category', categoryCtrl.createCategory)


//Afficher un client

router.get("/Category/:_id", categoryCtrl.getCategory);
router.get("/Category", categoryCtrl.getAllCategory);


// Modifier un client
router.put("/Category/:_id", categoryCtrl.updateCategory);
router.patch("/Category/:_id", categoryCtrl.updateCategory);



// Supprimer un Client
router.delete("/Category/:_id", categoryCtrl.deleteClient);



module.exports = router;