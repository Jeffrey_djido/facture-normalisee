const mongoose = require('mongoose');


const Category = mongoose.model('Category', {
    _id:{
        type: Number,
        autoIncrement:true
    },
    name:{
        type:String,
        required:true
    },
    partner_id:{
        type:Number,
        required:true
    },
    created_date:{
        type: Date,
        autoAdded:true
    },


});

module.exports = Category;