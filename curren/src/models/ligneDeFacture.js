 //CREER TABLE ligne de facture
 const mongoose = require('mongoose');
 const validator = require('validator');

 const ligneDeFacture = mongoose.model('ligneDeFacture',{
    _id:{
        type:Number,
        autoIncrement:true
    },
    article_id:{
        type:Number,
        required: true
    },
    invoice_id:{
        type: Number,
        required: true
    }, 
    vat_id:{
        type:Number,
        required:true
    },
    categoy_id:{
        type:Number,
        required:true
    },
    total_price:{
        type: String,
        required:true
    },
    quantity:{
        type: String,
        required:true
    },
    description:{
        type: String,
        optional: true
    }
});

module.exports = ligneDeFacture;
