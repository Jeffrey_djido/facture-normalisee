   //CREER TABLE Article 
const { text } = require('body-parser');
const mongoose = require('mongoose');
const validator = require('validator');
const  {float}  = require('webidl-conversions');

const Article = mongoose.model('Article',{
   
    Name:{ 
        type: String,
        required:true
    },
    barcode:{
        type: String,
        genereted:true
    },
    category_id:{
        type: Number,
        required:true
    },
    activated:{
        type: Boolean,
        choice: true || false,
    },
    quantity:{
        type: String,
        default : 0,
    },
    created_date:{
        type: Date,
        autoAdded: true,
    },
    partner_id:{
        type:Number,
        required:true,

    },
    description:{
        type:String,
        optional:true,
    },
    unit_price:{
        type: String,
        default : 0,
    },

    prodution_date:{
        type: Date,
        optional: true
    },
    expiration_date:{
        type: Date,
        optional: true
    }
    
});

module.exports = Article;