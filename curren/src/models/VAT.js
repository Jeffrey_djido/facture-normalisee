const mongoose = require('mongoose');
 const Vat = mongoose.model('Vat', {
    _id:{
        type:Number,
        autoIncrement:true

    },
    names:{
        type:String,
        required:true
    },
    value:{
        type:String,
        required:true
    },
 })