 //Gérer les Client
 const mongoose = require('mongoose');
 const validator = require('validator');

 const Client = mongoose.model('Client',{
     Nom_cli:{
        type: String,
        required: true
    },
    prenom_cli:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    adress:{
        type: String,
        required: true
    },
    Num_Tel:{
        type: String,
        required: true
    },
    
});
 
module.exports = Client;