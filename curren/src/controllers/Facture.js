const Facture = require('../models/facture');

// créer une facture
exports.createFacture = (req, res) => {
  const {_id, invoice_date,cancelled_date,created_date,status,code,type,user_id,partner_id,client_id,payed_date,validated_date,comment,total_excl_vat,total_vat,discount_percent,discount_amount,total_price} = req.body;
  // créer une nouvelle facture
  const newFacture = {
    _id,
    invoice_date,
    
    cancelled_date,
    created_date,
    status,
    code,
    type,
    user_id,
    partner_id,
    client_id,
    payed_date,
    validated_date,
    comment,
    total_excl_vat,
    total_vat,
    discount_percent,
    discount_amount,
    total_price,
     
  };

  // ajouter l'facture à la liste des facture
  Facture.create(newFacture, (err, facture) => {
      if (err) {
          return res.status(400).json({ msg: 'Impossible de créer la facture'+err });
      }
      res.json('facture créer');
  });
};

// lire une facture
/*exports.getFacture = (req, res) => {
  const { id } = req.params;
  Facture.find(id, (err, facture) => {
      if (err) {
          return res.status(404).json({ msg: 'facture non trouvé' });
      }
      res.json(facture);
  });
};*/


exports.getFacture = (req, res) => {
  const { _id } = req.params;
  Facture.findOne({_id:_id}, (err, facture) => {
      if (err) {
          return res.status(404).json({ msg: 'facture non trouvé '+err });
      }
      res.json(facture);
  }); 
};


//lire all facture


exports.getAllFacture = (req, res, next) => {
  const { id } = req.params; 
	Facture.find()
		.then((facture) => { 
			res.status(200).json(facture);
		})
		.catch((error) => {
			res.status(400).json({ error: error });
		});
}

// modifier un facture
exports.updateFacture = (req, res) => {
  const { _id } = req.params;
  const { invoice_date,cancelled_date,created_date,status,code,type,user_id,partner_id,client_id,payed_date,validated_date,comment,total_excl_vat,total_vat,discount_percent,discount_amount,total_price } = req.body;

  const updatedFacture = {
    _id,
    invoice_date, 
    cancelled_date,
    created_date,
    status,
    code,
    type,
    user_id,
    partner_id,
    client_id,
    payed_date,
    validated_date,
    comment,
    total_excl_vat,
    total_vat,
    discount_percent,
    discount_amount,
    total_price,
  };

  Facture.findOneAndUpdate({_id:_id}, { $set: updatedFacture }, { new: true }, (err, facture) => {
      if (err) {
          return res.status(404).json({ msg: 'Facture non trouvé'+err });
      }
      res.json('facture modifier');
     console.log('facture modifier');
  });
};

// supprimer une facture
exports.deleteFacture = (req, res) => {
  const { _id } = req.params;

  Facture.findOneAndRemove({_id:_id}, (err, facture) => {
      if (err) {
          return res.status(404).json({ msg: 'facture non trouvé'+err });
      }
      res.json({ msg: 'facture supprimé' });
  });
};




















/*
// Ajouter une facture
exports.add = (id_fact, date_fact, paiement, Prix_total, Prix_h_taxe, Prix_TTC, Commentaire) => {
  Facture.create({
    id_fact,
    date_fact,
    paiement,
    Prix_total,
    Prix_h_taxe,
    Prix_TTC,
    Commentaire
  });
};

// Modifier une facture
exports.update = (id_fact, date_fact, paiement, Prix_total, Prix_h_taxe, Prix_TTC, Commentaire) => {
  Facture.update({
    date_fact,
    paiement,
    Prix_total,
    Prix_h_taxe,
    Prix_TTC,
    Commentaire
  },
  {
    where: {
      id_fact
    }
  });
};

// Supprimer une facture
exports.delete = (id_fact) => {
  Facture.destroy({
    where: {
      id_fact
    }
  });
};
*/