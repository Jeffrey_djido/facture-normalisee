const Client= require('../models/Client');
const lignesFacture = require('../models/ligneDeFacture');
const moyensPaiement = require('../models/moyenDePaiement');
const User = require('../models/user')

// ajouter un information client
exports.createClient = (req, res) => {
    const {Nom_cli, prenom_cli, email, adress, Num_cli} = req.body;
    // créer une nouvelle facture
    const newClient = {
      Nom_cli,
      prenom_cli,
      email,
      adress,
      Num_cli
    };
  
    // ajouter le client à la liste des Clients
    Client.create(newClient, (err,  client) => {
        if (err) {
            return res.status(400).json({ msg: 'Impossible de créer le Client' });
        }
        res.json('Client créer');
    });
};
  


//ajouter une ligne de facture

exports.createLigneFacture = (req, res) => {
    const {id_com, date_com, descrip} = req.body;
    // créer une nouvelle ligne de facture
    const newLigneFacture = {
        id_com, 
        date_com, 
        descrip
    };

    // ajouter la ligne de facture à la liste des lignes de facture
    lignesFacture.create(newLigneFacture, (err, ligneFacture) => {
        if (err) {
            return res.status(400).json({ msg: 'Impossible de créer la ligne de facture' });
        }
        res.json(ligneFacture);
    });
};



// ajouter un moyen de paiement

exports.getMoyenDePaiement = (req, res) => {
    const { id } = req.params;
    moyensPaiement.findOne(id, (err, MoyenDePaiement) => {
        if (err) {
            return res.status(404).json({ msg: 'MoyenDePaiement non trouvé' });
        }
        res.json(MoyenDePaiement);
    });
  };
