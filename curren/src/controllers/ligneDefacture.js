const lignesFacture = require('../models/ligneDeFacture');

// ajouter une ligne de facture
exports.createLigneFacture = (req, res) => {
    const {_id,article_id,
        invoice_id,
        vat_id,
        category_id,
        total_price,
        quantity,
        description} = req.body;
    // créer une nouvelle ligne de facture
    const newLigneFacture = {
        _id,
        article_id,
        invoice_id,
        vat_id,
        category_id,
        total_price,
        quantity,
        description

    };

    // ajouter la ligne de facture à la liste des lignes de facture
    lignesFacture.create(newLigneFacture, (err, ligneFacture) => {
        if (err) {
            return res.status(400).json({ msg: 'Impossible de créer la ligne de facture' });
        }
        res.json(ligneFacture);
    });
};


// lire une ligne de facture
exports.getLigneFacture = (req, res) => {
    const { _id } = req.params;
    lignesFacture.findOne({_id:_id}, (err, ligneFacture) => {
        if (err) {
            return res.status(404).json({ msg: 'ligne de facture non trouvé'+err });
        }
        res.json(ligneFacture);
    });
  };
  
  //lire all ligne de facture
  
  
  exports.getAllLigneFacture = (req, res, next) => {
    const { _id } = req.params; 
    lignesFacture.find()
          .then((ligneFacture) => { 
              res.status(200).json(ligneFacture);
          })
          .catch((error) => {
              res.status(400).json({ error: error });
          });
  }



// modifier une ligne de facture
exports.updateLigneFacture = (req, res) => {
    const { _id } = req.params;
    const { article_id,
        invoice_id,
        vat_id,
        category_id,
        total_price,
        quantity,
        description} = req.body;

    const updatedLigneFacture = {
        _id,
        article_id,
        invoice_id,
        vat_id,
        category_id,
        total_price,
        quantity,
        description
    };

    lignesFacture.findOneAndUpdate({_id:_id}, { $set: updatedLigneFacture }, { new: true }, (err, ligneFacture) => {
        if (err) {
            return res.status(404).json({ msg: 'Ligne de facture non trouvée'+err });
        }
        res.json('ligne de Facture modifier ');
    });
};

// supprimer une ligne de facture
exports.deleteLigneFacture = (req, res) => {
    const { _id } = req.params;

    lignesFacture.findOneAndRemove({_id:_id}, (err, ligneFacture) => {
        if (err) {
            return res.status(404).json({ msg: 'Ligne de facture non trouvée'+err });
        }
        res.json('Ligne de facture supprimée')
    });
};