
//const Client= require('../models/client');
const Client= require('../models/Client');

// créer une facture
exports.createClient = (req, res) => {
  const {Nom_cli, prenom_cli, email, adress, Num_cli} = req.body;
  // créer une nouvelle facture
  const newClient = {
    Nom_cli,
    prenom_cli,
    email,
    adress,
    Num_cli
  };

  // ajouter l'facture à la liste des facture
  Client.create(newClient, (err,  client) => {
      if (err) {
          return res.status(400).json({ msg: 'Impossible de créer le Client' });
      }
      res.json('Client créer');
  });
};

// lire un  Client
exports.getClient = (req, res) => {
  const { _id } = req.params;
  Client.findOne({_id:_id}, (err,  client) => {
      if (err) {
          return res.status(404).json({ msg: ' Client non trouvé'+err });
      }
      res.json( client);
  });
};

//lire all facture


exports.getAllClient = (req, res, next) => {
  const { id } = req.params; 
    Client.find()
		.then((Client) => { 
			res.status(200).json(client);
		})
		.catch((error) => {
			res.status(400).json({ error: error });
		});
}

// modifier un facture
exports.updateClient = (req, res) => {
  const { _id } = req.params;
  const { Nom_cli, prenom_cli, email, adress, Num_cli } = req.body;

  const updatedClient = {
    Nom_cli,
    prenom_cli,
    email,
    adress,
    Num_cli
  };

  Client.findOneAndUpdate({_id:_id}, { $set: updatedClient }, { new: true }, (err, client) => {
      if (err) {
          return res.status(404).json({ msg: 'Client non trouvé'+err });
      }
      res.json('Client modifier');
     console.log('Client modifier');
  });
};

// supprimer une Client
exports.deleteClient = (req, res) => {
  const { _id } = req.params;

  Client.findOneAndRemove({_id:_id}, (err, client) => {
      if (err) {
          return res.status(404).json({ msg: 'client non trouvé'+err });
      }
      res.json({ msg: 'Client supprimé' });
  });
};
