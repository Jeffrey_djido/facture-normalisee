const balanceCommerciale = require('../models/balanceCommerciale.js');


/*exports.add = (req, res) => {
  const { id_bal, libelle_bal } = req.body;
  balanceCommerciale.add(id_bal, libelle_bal);
  res.send('Balance commerciale ajoutée avec succès !');
};*/
exports.createBalance = (req, res) => {
  const {vol_exp,vol_imp,term_echange } = req.body;
  // créer balance commerciale
  const newBalance = {
    vol_exp, 
  vol_imp,
  term_echange 
  };

 // Ajouter une balance commerciale
 balanceCommerciale.create(newBalance, (err, Balance) => {
      if (err) {
          return res.status(400).json({ msg: 'Impossible de créer la balance commerciale' });
      }
      res.json('Balance commerciale créer');
      
  });
};
// lire une balance commerciale
exports.getBalance = (req, res) => {
  const { _id } = req.params;
  balanceCommerciale.findOne({_id:_id}, (err, Balance ) => {
      if (err) {
          return res.status(404).json({ msg: 'Balance commerciale non trouvé'+err });
      }
      res.json(Balance);
  });
};

//lire all balance commerciale
exports.getAllBalance = (req, res, next) => {
  const { id } = req.params; 
	balanceCommerciale.find()
		.then((Balance) => { 
			res.status(200).json(Balance);
		})
		.catch((error) => {
			res.status(400).json({ error: error });
		});
}

// modifier une balance Commerciale 
exports.updateBalance = (req, res) => {
  const { _id } = req.params;
  const { vol_exp,vol_imp,term_echange  } = req.body;

  const updatedBalance = {
    vol_exp,
    vol_imp,
    term_echange 
   
  };

  balanceCommerciale.findOneAndUpdate({_id:_id}, { $set: updatedBalance }, { new: true }, (err, Balance) => {
      if (err) {
          return res.status(404).json({ msg: 'Balance commerciale non trouvé'+err });
      }
      res.json('Balance Commerciale modifier');
     console.log('Balance commerciale modifier');
  });
};

// supprimer une Balance Commerciale
exports.deleteBalance = (req, res) => {
  const { _id } = req.params;

  balanceCommerciale.findOneAndRemove({_id:_id}, (err, Balance) => {
      if (err) {
          return res.status(404).json({ msg: 'Balance commmerciale non trouvé'+err });
      }
      res.json('Balance Commerciale supprimer');
  });
};













/* Modifier une balance commerciale
exports.update = (req, res) => {
  const { id_bal } = req.params;
  const { libelle_bal } = req.body;
  balanceCommerciale.update(id_bal, libelle_bal);
  res.send('Balance commerciale mise à jour avec succès !');
};

// Supprimer une balance commerciale
exports.delete = (req, res) => {
  const { id_bal } = req.params;
  balanceCommerciale.delete(id_bal);
  res.send('Balance commerciale supprimée avec succès !');
};*/