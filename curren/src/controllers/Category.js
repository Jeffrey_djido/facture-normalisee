
//const Client= require('../models/client');
const Category= require('../models/Category');


// créer une facture
exports.createCategory = (req, res) => {
  const {name, partner, created_date} = req.body;
  // créer une nouvelle facture
  const newCategoty = {
    name,
    partner,
    created_date,
    
  };

  // ajouter l'facture à la liste des facture
  Category.create(newCategoty, (err,  Category) => {
      if (err) {
          return res.status(400).json({ msg: 'Impossible de créer la categorie' });
      }
      res.json('Categorie créer');
  });
};

// lire un  Client
exports.getCategory = (req, res) => {
  const { _id } = req.params;
  Category.findOne({_id:_id}, (err,  Category) => {
      if (err) {
          return res.status(404).json({ msg: ' Category non trouvé'+err });
      }
      res.json(Category );
  });
};

//lire all facture


exports.getAllCategory = (req, res, next) => {
  const { id } = req.params; 
    Category.find()
		.then((Category) => { 
			res.status(200).json(Category);
		})
		.catch((error) => {
			res.status(400).json({ error: error });
		});
}

// modifier un facture
exports.updateCategory = (req, res) => {
  const { _id } = req.params;
  const { name, partner, created_date} = req.body;

  const updatedCategory = {
    name,
    partner,
    created_date,
  };

  Category.findOneAndUpdate({_id:_id}, { $set: updatedCategory }, { new: true }, (err, Category) => {
      if (err) {
          return res.status(404).json({ msg: 'Categorie non trouvé'+err });
      }
      res.json('Categorie modifier');
     console.log('Categorie modifier');
  });
};

// supprimer une Client
exports.deleteCategorie = (req, res) => {
  const { _id } = req.params;

  Category.findOneAndRemove({_id:_id}, (err, Category) => {
      if (err) {
          return res.status(404).json({ msg: 'Categorie non trouvé'+err });
      }
      res.json({ msg: 'Categorie supprimé' });
  });
};
