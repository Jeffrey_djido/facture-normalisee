
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
//const server = require('./server');
const userRoutes = require('./src/routes/user');
const articleRoutes = require('./src/routes/article');
const balanceCommercialeRoutes = require('./src/routes/balanceCommerciale');
const factureRoutes  = require('./src/routes/facture');
const ligneDeFactureRoutes = require('./src/routes/ligneDeFacture');
const moyenDePaiementRoutes = require('./src/routes/moyenDePaiement');
const etablirFactureRoutes = require('./src/routes/etablirFacture');
const app = express();

/*const swaggerDocument = require('./swagger.json');
const swaggerDocumentFact = require('./swagger2.json');


const useBasePath = true;
const middlewareObj = {
    'middleware-name1': require('./src/middlewares/middleware-name1'),
    //'middleware-name2': require('./src/middlewares/middleware-name2')
};
swagger.setUpRoutes(middlewareObj, app, swaggerDocument, useBasePath);
swagger.setUpRoutes(middlewareObj, app, swaggerDocumentFact, useBasePath);*/




//Helmet aide à sécuriser les applications Express en définissant divers en-têtes HTTP
const helmet = require("helmet");

// middleware qui nettoie les données fournies par l'utilisateur pour empêcher l'injection d'opérateur MongoDB.
const mongoSanitize = require('express-mongo-sanitize');


const { connectDb } =  require('./src/services/mongoose');
const User = require('./src/models/user');
const Article = require('./src/models/article');
const balanceCommerciale = require('./src/models/balanceCommerciale');
const Facture = require('./src/models/facture');
const ligneDeFacture = require('./src/models/ligneDeFacture');
const moyenDePaiement = require('./src/models/moyenDePaiement');

connectDb().catch(err => console.log(err));


app.use(express.json());
app.use(bodyParser.json()); 

app.use( userRoutes);
app.use( articleRoutes);
app.use( balanceCommercialeRoutes);
app.use( factureRoutes);
app.use( ligneDeFactureRoutes);
app.use( moyenDePaiementRoutes);
app.use( etablirFactureRoutes);


app.post('/box', async (req, res, next) => {
    const user = new User(req.body);
    const saveUser = await  user.save();
    res.send('Utilisateur enrégistré')
});

/*app.post('/article', async (req, res, next) => {
    const article = new Article(req.body);
    const saveArticle = await  article.save();
    res.send('Article enrégistré')
});
//app.use('/article', articleRoutes)
app.post('/balanceCommerciale', async (req, res, next) => {
    const balance = new balanceCommerciale(req.body);
    const saveBalanceCommerciale = await  balance.save();
    res.send('balance Commerciale enrégistré')
});

app.post('/facture', async (req, res, next) => {
    const facture = new Facture(req.body);
    const saveFacture = await facture.save();
    res.send('Facture enrégistré')
});
app.post('/ligneDeFacture', async (req, res, next) => {
    const lineFacture = new ligneDeFacture(req.body);
    const savelineFacture = await lineFacture.save();
    res.send('ligne De Facture enrégistré')
});
app.post('/moyenDePaiement', async (req, res, next) => {
    const Paiement = new moyenDePaiement(req.body);
    const saveMoyenDePaiement = await Paiement.save();
    res.send('ligne De Facture enrégistré')
});
*/

const port = process.env.PORT || 4000;
app.listen(port, () => {
    console.log(`le serveur est lancé à: http://localhost:${port}`);
})
app.use(helmet());
app.use(mongoSanitize());


app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
	);
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PUT, DELETE, PATCH, OPTIONS"
    );
    next();
});




module.exports = app;



/*app.use(express.json());

app.post('/todos', async (req, res, next) => {
    const user = new User(req.body);
    const saveUser = await  user.save();
    res.send('saveUser');
});

app.listen(port, () => {
    console.log(`le serveur est lancé à: http://localhost:${port}`);
});*/

 

