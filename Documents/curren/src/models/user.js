const mongoose = require('mongoose');
const validator = require("validator");
//const uniqueValidator = require('mongoose-unique-validator');
const path = require('path');
const { kMaxLength } = require('buffer');
//const router = require('./src/routes/user');


const userSchema = mongoose.Schema({
    _id:{type: Number, autoIncrement: true,},
    contact_lastname: { type: String, required: true, },
    contact_firstname: { type: String, required: true, },
   
    company_business_register: { type: Number, optional: true,  },
    company_number: { type: Number, required: true,  },    
    email: { type: String, required: true,},
  
  mobile_number:{
    type: String,
    optional: true
},
landine_number:{
    type: String,
    optional: true
},
date_joined:{
    type: Date,
    autoAdded: true
},
company_name:{
    type: String,
    required: true
},
partner_id:{
    type: Number,
    required: true
},  
});

module.exports = mongoose.model("User", userSchema);

//userSchema.plugin(uniqueValidator);


/*validate(v) {
     if (!validator.isLowercase(v))
         
     throw new Error('Doit contenir des lettres minuscules');
 },
 validate(v) {
     if (!validator.isNumeric(v))
         
     throw new Error('doit avoir au moins 1 chiffre');
 },*/
 

/*const firstPerson = new userSchema({
    nom: 'Bio',
    prenom:'jeff',
    Num_Tel: 51246628,
    Num_MF_C:32154689745632,
    Num_RC_C: 000012436547,
   email: 'jeff@example.fr',
   password: 'password'
});
const secondPerson = new userSchema({
    nom: 'Biola',
    prenom:'fredis',
    Num_Tel: 51246628,
    Num_MF_C:32154689745632,
    Num_RC_C: 000012436547,
    email: 'jefferson@pause.com',
    password: 'Password123'
});

const firstSave = await firstPerson.save();
/*const secondSave = await secondPerson.save();
console.log(firstPerson, secondPerson);*/
