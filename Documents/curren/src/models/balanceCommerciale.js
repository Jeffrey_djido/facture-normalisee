//CREER TABLE balance commerciale
const mongoose = require('mongoose');
const validator = require('validator');

const balanceCommerciale = mongoose.model('balanceCommerciale',{
    
     
    vol_exp:{
        type: String,
        required: true
    },
    vol_imp:{
        type: String,
        required: true
    },
    term_echange:{
        type: String,
        required: true
    },

}); 

module.exports = balanceCommerciale;