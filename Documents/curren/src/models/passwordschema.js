const passwordValidator = require('password-validator'); 


const passwordSchema = new passwordValidator();


passwordSchema
    .is().min(8)                                    // longueur Minimum 8
    .is().max(100)                                  // longueur Maximum 100
    .has().uppercase()                              // Doit contenir des lettres majuscules
    .has().lowercase()                              // Doit contenir des lettres minuscules
    .has().digits(1)                                // doit avoir au moins 1 chiffre
    .has().not().spaces()                           // ne doit pas avoir de l'espaces
    .is().not().oneOf(['Passw0rd', 'Password123']);// liste noire de ces valeurs


// Validate against a password string
console.log(passwordSchema.validate('validPASS123'));
// => true
console.log(passwordSchema.validate('invalidPASS'));
// => false

// Get a full list of rules which failed
console.log(passwordSchema.validate('joke', { list: true }));
// => [ 'min', 'uppercase', 'digits' ]

module.exports = passwordSchema;