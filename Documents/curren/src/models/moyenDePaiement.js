  //CREER TABLE Moyen de paiement
  const mongoose = require('mongoose');
  const validator = require('validator');

  const moyenDePaiement = mongoose.model('moyenDePaiement',{
    Libellé_Paie:{
        type: String,
        required: true
    },
    Method_Paie:{
        type: String,
        required: true
    },
});

module.exports = moyenDePaiement;