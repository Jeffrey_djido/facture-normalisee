
const express = require('express');
const router = express.Router();
const BalanceCtrl = require('../controllers/balanceCommerciale');

// Ajouter une balance commerciale
router.post('/balanceCommerciale', BalanceCtrl.createBalance)


//Afficher une blance commerciale

router.get("/balanceCommerciale/:_id", BalanceCtrl.getBalance);
router.get("/balanceCommerciale", BalanceCtrl.getAllBalance);


// Modifier une balance commerciale
router.put("/balanceCommerciale/:_id", BalanceCtrl.updateBalance);
router.patch("/balanceCommerciale/:_id", BalanceCtrl.updateBalance);



// Supprimer une balance commerciale
router.delete("/balanceCommerciale/:_id", BalanceCtrl.deleteBalance);



module.exports = router;