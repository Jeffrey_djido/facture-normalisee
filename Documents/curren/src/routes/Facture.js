const express = require('express');
const router = express.Router();
const factureCtrl = require('../controllers/Facture');
const ligneDeFactureCtrl = require("../controllers/ligneDefacture")



//creer une facture 
router.post("/facture", factureCtrl.createFacture);

//CRUD ligne de facture
router.post("/ligneDeFacture", ligneDeFactureCtrl.createLigneFacture);
router.get("/ligneDeFacture", ligneDeFactureCtrl.getLigneFacture)
router.put("/ligneDeFacture", ligneDeFactureCtrl.updateLigneFacture );
router.delete("/ligneDeFacture", ligneDeFactureCtrl.deleteLigneFacture);




//Afficher une facture

router.get("/facture/:_id", factureCtrl.getFacture);
router.get("/facture", factureCtrl.getAllFacture);

// Modifier une facture
router.put("/facture/:_id", factureCtrl.updateFacture);
router.patch("/facture/:_id", factureCtrl.updateFacture);


// Supprimer une facture

router.delete("/facture/:_id", factureCtrl.deleteFacture);


module.exports = router;