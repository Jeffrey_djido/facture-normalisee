
const express = require('express');
const router = express.Router();
const ObjectID = require('mongoose').Types.ObjectId;
const Article = require('../models/article');
const ArticleCtrl = require('../controllers/article');
router.use(express.json());


const app = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// Extended: https://swagger.io/specification/#infoObject

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Article API',
            description: "Article API information",
            contact:{
                name:"Amazing Developer"
            },
            servers: ["http://localhost:4000"]
        }
    },
    //['./routes/*.js']
    apis:["article.js"]
}

const swaggerDocs = swaggerJsDoc(swaggerOptions)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));



// créer un article
router.post("/article", ArticleCtrl.createArticle);
/*router.post('/article', async (req, res, next) => {
    const article = new Article(req.body);
    const saveArticle = await  article.save();
    res.send('Article enrégistré');

    // ajouter l'article à la liste des articles
    article.push(Article);

    //const saveArticle =  article.save();
    //res.send('Article enrégistré')

    res.json(Article);

      
    
});*/

// lire un article
router.get("/article/:_id", ArticleCtrl.getArticle);




/*('/article', (req, res) => {
    const { Nom_Art } = req.params;

    const article = Article.find( Nom_Art);

    if (!article) {
        return res.status(404).json({ msg: 'Article non trouvé' });
    }

    res.json(article);
});*/

// modifier un article
router.put("/article/:_id", ArticleCtrl.updateArticle);
router.patch("/article/:_id", ArticleCtrl.updateArticle);
/*router.put('/article/:id', (req, res) => {
    const { id } = req.params;
    const { Nom_Art, Prix_unit, Qte_stock, Date_Fab, Date_Exp } = req.body;

    const articleIndex = Article.findIndex(a => a.id_Art == id);

    if (articleIndex === -1) {
        return res.status(404).json({ msg: 'Article non trouvé' });
    }

    const updatedArticle = {
        id_Art: Article[articleIndex].id_Art,
        Nom_Art,
        Prix_unit,
        Qte_stock,
        Date_Fab,
        Date_Exp
    };

    Article[articleIndex] = updatedArticle;

    res.json(updatedArticle);
});*/

// supprimer un article
router.delete("/article/:_id", ArticleCtrl.deleteArticle);
/*router.delete('/article/:id', (req, res) => {
    const { id } = req.params;

    const articleIndex = Article.findIndex(a => a.id_Art == id);

    if (articleIndex === -1) {
        return res.status(404).json({ msg: 'Article non trouvé' });
    }

    Article.splice(articleIndex, 1);

    res.json({ msg: 'Article supprimé' });
});*/

module.exports = router;