const express = require('express');
const router = express.Router();
const ligneDeFactureCtrl = require("../controllers/ligneDefacture")
const etablirFactureCtrl = require("../controllers/etablirFacture")
const clientCtrl = require('../controllers/client');
const ligneDeFacture = require('../models/ligneDeFacture');

//ajout de client
router.post('/client', clientCtrl.createClient);
router.get("/client/:_id", clientCtrl.getClient);
router.put("/client/:_id", clientCtrl.updateClient );
router.delete("/client/:_id", clientCtrl.deleteClient);


// CRUD ligne de facture
router.post("/ligneDefacture", ligneDeFactureCtrl.createLigneFacture);
router.get("/ligneDeFacture", ligneDeFactureCtrl.getLigneFacture)
router.put("/ligneDeFacture", ligneDeFactureCtrl.updateLigneFacture );
router.delete("/ligneDeFacture", ligneDeFactureCtrl.deleteLigneFacture);


//ajout de moyen de paiement
router.get("/moyenPaiement/:_id", etablirFactureCtrl.getMoyenDePaiement);

module.exports = router;














