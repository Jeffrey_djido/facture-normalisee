
const article = require('../models/article');

// créer un article

exports.createArticle = (req, res) => {
    const {  
        Name,
        barcode,
        category_id,
        activated,
        created_date,
        partner_id,
        description,
        quantity,
        unit_price,
        production_date,
        expiration_date} = req.body;
    // créer un nouvel article
    const newArticle = {
        
        Name,
        barcode,
        category_id,
        activated,
        created_date,
        partner_id,
        description,
        quantity,
        unit_price,
        production_date,
        expiration_date


    };

    // ajouter l'article à la liste des articles
    article.create(newArticle, (err, Article) => {
        if (err) {
            return res.status(400).json({ msg: 'Impossible de créer l\'article' +err });
        }
        res.json('Article créer');
        
    });
};

// lire un article
exports.getArticle = (req, res) => {
    const { _id } = req.params;

    article.findOne({_id:_id}, (err, article) => {
        if (err) {
            return res.status(404).json({ msg: 'Article non trouvé'+err });
        }
        res.json(article);
        console.log(article)
    });
};


// modifier un article

/*exports.updateArticle = (req, res) => {
    const { Nom_Art } = req.params;
    const { Prix_unit, Qte_stock, Date_Fab, Date_Exp } = req.body;

    const updatedArticle = {
        
        Nom_Art, 
        Prix_unit, 
        Qte_stock, 
        Date_Fab, 
        Date_Exp
    };
    article.findByIdAndUpdate(Nom_Art, { $set: updatedArticle }, { new: true },(err, article) => {
        if (err) {
            return res.status(404).json({ msg: 'Article non trouvé' });
        }
            
            res.json(article);        
         res.send('aricle mise à jour avec succès !');
    });  
    //article.update( Nom_Art,Prix_unit, Qte_stock, Date_Fab, Date_Exp);
   
  };*/

exports.updateArticle = (req, res) => {
    const { _id } = req.params;
    const {   
        Name,
        barcode,
        category_id,
        activated,
        created_date,
        partner_id,
        description,
        quantity,
        unit_price,
        production_date,
        expiration_date } = req.body;

    const updatedArticle = {
        
        Name,
        barcode,
        category_id,
        activated,
        created_date,
        partner_id,
        description,
        quantity,
        unit_price,
        production_date,
        expiration_date
    };

    article.findOneAndUpdate({_id:_id}, { $set: updatedArticle }, { new: true }, (err, article) => {
        if (err) {
            return res.status(404).json({ msg: 'Article non trouvé'+err });
        }
            res.json(article);
            res.json('article modifier avec success');
            
       
    });  
};

// supprimer un article
exports.deleteArticle = (req, res) => {
    const { _id } = req.params;

    article.findOneAndRemove({_id:_id}, (err, article) => {
        if (err) {
            return res.status(404).json({ msg: 'Article non trouvé'+err });
        }
        res.json({ msg: 'Article supprimé' });
    });
};