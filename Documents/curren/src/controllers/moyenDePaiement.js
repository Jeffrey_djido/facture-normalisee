const moyensPaiement = require('../models/moyenDePaiement');

// ajouter un moyen de paiement
exports.createMoyenDePaiement = (req, res) => {
    const {id_Paie, libelle_Paie, method_Paie} = req.body;
    // créer un nouveau moyen de paiement
    const newMoyenDePaiement = {
        id_Paie, 
        libelle_Paie, 
        method_Paie
    };

    // ajouter le moyen de paiement à la liste des moyens de paiement
    moyensPaiement.create(newMoyenDePaiement, (err, moyenPaiement) => {
        if (err) {
            return res.status(400).json({ msg: 'Impossible de créer le moyen de paiement' });
        }
        res.json(moyenPaiement);
    });
};

// lire un MoyenDePaiement
exports.getMoyenDePaiement = (req, res) => {
    const { id } = req.params;
    moyensPaiement.findOne(id, (err, MoyenDePaiement) => {
        if (err) {
            return res.status(404).json({ msg: 'MoyenDePaiement non trouvé' });
        }
        res.json(MoyenDePaiement);
    });
  };


// modifier un moyen de paiement
exports.updateMoyenDePaiement = (req, res) => {
    const { id } = req.params;
    const { libelle_Paie, method_Paie } = req.body;

    const updatedMoyenPaiement = {
        libelle_Paie,
        method_Paie
    };

    moyensPaiement.findOneAndUpdate(id, { $set: updatedMoyenPaiement }, { new: true }, (err, moyenPaiement) => {
        if (err) {
            return res.status(404).json({ msg: 'Moyen de paiement non trouvé' });
        }
        res.json(moyenPaiement);
    });
};

// supprimer un moyen de paiement
exports.deleteMoyenDePaiement = (req, res) => {
    const { id } = req.params;

    moyensPaiement.findOneAndRemove(id, (err, moyenPaiement) => {
        if (err) {
            return res.status(404).json({ msg: 'Moyen de paiement non trouvé' });
        }
        res.json({ msg: 'Moyen de paiement supprimé' });
    });
};